//
//  API.swift
//  prudentialgoals
//
//  Created by Chaitanya Shah on 2017-07-13.
//  Copyright © 2017 Swish Labs. All rights reserved.
//

import Foundation

struct API {
    // MARK: - Paths
    
    static let hostUrl = Bundle.main.string(for: "Base URL") 
    static let baseUrl = hostUrl + "/api/v1"
    
    struct users {
        static let signupPath = "\(baseUrl)/users/sign_up"
        static let loginPath = "\(baseUrl)/users/sign_in"
        static let logoutPath = "\(baseUrl)/users/sign_out"
        static let forgotPasswordPath = "\(baseUrl)/users/forgot_password"
        static let synapsePath = "\(baseUrl)/users/synapse"
        static let primaryAccountPath = "\(baseUrl)/users/synapse/primary"
        static let financialInfosPath = "\(baseUrl)/users/financial_infos"
        static let goalsPath = "\(baseUrl)/users/goals"
        static let fundingSchedulesPath = "\(baseUrl)/users/funding_schedules"
        static let scheduledPath = "\(baseUrl)/users/funding_schedules/scheduled"
        static let devicePath = "\(baseUrl)/users/device"
        static let pausePath = "\(baseUrl)/users/pause"
        static let closeAccountPath = "\(baseUrl)/users/close_account"
        static let ccTransferPath = "\(baseUrl)/users/next_cc_transfer"
        static let retirementCalculatorPath = "\(baseUrl)/users/retirement"
        static let surveyPath = "\(baseUrl)/users/survey"
    }
    
    struct notifications {
        static let notificationsPath = "\(baseUrl)/notifications"
        static let readNotificationsPath = "\(baseUrl)/notifications/read"
        static let sageMessagesPath = "\(baseUrl)/notifications/sage_messages"
        static let readSageMessagePath = "\(baseUrl)/notifications/read_sage_message"
        static let errorStatesPath = "\(baseUrl)/notifications/error_state_messages"
    }
    
    struct accounts {
        static let accountsPath = "\(baseUrl)/accounts"
    }
    
    struct plaidToken {
        static let tokenPath = "\(baseUrl)/plaid_token"
    }
    
    struct plaid {
        static let status = "\(baseUrl)/plaid/status"
        static let removeInstitutionPath = "\(baseUrl)/plaid/institution"
    }
    
    struct admin {
        static let goalsPath = "\(baseUrl)/admin/goals"
    }
    
    // MARK: - Fields
    
    struct ResultsFields {
        static let success = "success"
        static let error = "error"
        static let suspended = "suspended"
    }
    
    struct FilterFields {
        static let goals = "goals"
        static let finacialTransactions = "financial_transactions"
        static let accounts = "accounts"
        static let goal = "goal"
        static let transactions = "transactions"
    }
    
    struct SharedAPIFields {
        static let accessToken = "access_token"
        static let email = "email"
        static let password = "password"
        static let value = "value"
        static let output = "output"
        static let recommendedGoals = "recommended_goals"
        static let id = "id"
    }
    
    struct UserGoalsFields {
        static let goalId = "goal_id"
        static let endDate = "end_date"
        static let dueDate = "due_date"
        static let totalAmountCents = "total_amount_cents"
        static let weeklyAmountCents = "weekly_amount_cents"
        static let savingsAmountCents = "savings_amount_cents"
        static let description = "description"
        static let userAccountId = "user_account_id"
        static let retirementAccountIds = "retirement_account_ids"
    }
    
    struct DeleteTransactionFields {
        static let transactionId = "goal_funding_schedule_id"
        static let date = "date"
    }
    
    struct NotificationsFields {
        static let notifications = "notifications"
        static let sageMessages = "sage_messages"
        static let errorStates = "error_state_messages"
    }
    
    struct SignUpFields {
        static let confirmPassword = "confirm_password"
    }
    
    struct PlaidFields {
        static let publicToken = "public_token"
        static let userInstitutionId = "user_institution_id"
        static let status = "status"
        static let isIncomeCalculated = "is_income_calculated"
    }
    
    struct PrimaryAccountFields {
        static let accountId = "account_id"
    }
    
    struct DeviceFields {
        static let type = "type"
        static let apple = "apple"
        static let deviceToken = "device_token"
    }
    
    struct FinancialInfoFields {
        static let recalculate = "recalculate"
        static let financialInfos = "financial_infos"
        static let financialInfoId = "financial_info_id"
        static let weeklyAmountCents = "weekly_amount_cents"
    }
    
    struct AvailableGoalFields {
        static let name = "name"
        static let description = "description"
        static let amountMessage = "goal_amount_message"
        static let dateMessage = "goal_date_message"
        static let message = "message"
        static let iconPath = "icon_type"
    }
    
    enum AvailableGoalIds: Int {
        case CreditCard = 1
        case RainyDay = 2
        case Loan = 3
        case Renovation = 4
        case Retirement = 6
        case Car = 7
        case House = 8
        case Education = 9
        case Wedding = 10
        case Baby = 11
        case Custom = 12
        case Vacation = 13
    }
    
    enum FinancialInfoIds: Int {
        case Income = 1
        case Budget = 3
        case Goals = 4
        case Leftover = 5
    }
    
    struct AccountTypes {
        static let prudentialGoals = ""
        static let depository = "depository"
        static let credit = "credit"
        static let brokerage = "brokerage"
        static let loan = "loan"
    }
    
    struct NextCCTransferFields {
        static let date = "date"
        static let amountCents = "amount_cents"
    }
    
    struct RetirementCalculatorFields {
        static let weeklySalaryCents = "weekly_salary_in_cents"
        static let retirementAmountCents = "total_savings_needed_at_retirement_in_cents"
    }
    
    struct SurveyTypes {
        static let lifeStage = "life_stage"
        static let rainyDay = "rainy_day_fund"
        static let kids = "kids"
        static let loans = "pay_off_loans"
        static let creditCard = "pay_off_creditcards"
    }
    
    struct ErrorEventTypes {
        static let lowBalance = "low_balance"
        static let linkAccount = "link_account"
    }
    
}
