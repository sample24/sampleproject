//
//  AlamofireManager.swift
//  prudentialgoals
//
//  Created by Przemysław Skowronek on 22/02/2018.
//  Copyright © 2018 Switchback Ventures. All rights reserved.
//

import Alamofire

struct AlamofireManager {

    static var requestId = 1

    static func request(
        _ url: URLConvertible,
        method: HTTPMethod = .get,
        parameters: Parameters? = nil,
        encoding: ParameterEncoding = URLEncoding.default,
        headers: HTTPHeaders? = nil,
        completionHandler: @escaping (DataResponse<Any>) -> Void)
    {
        let number = requestId
        requestId += 1
        let start = Date()
        Alamofire.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).responseJSON(completionHandler: { (data) in
            let end = Date()
            let interval = end.timeIntervalSince(start)
            print("AlamofireManager - finished request: url - \(url) \(number) time:\(interval)")
            completionHandler(data)
        })
    }
    
}
