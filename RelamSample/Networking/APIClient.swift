//
//  APIClient.swift
//  prudentialgoals
//
//  Created by Chaitanya Shah on 2017-08-07.
//  Copyright © 2017 Swish Labs. All rights reserved.
//

import RealmSwift
import Alamofire

class APIClient {
    static let shared = APIClient()
    var availableGoals: [[String: Any]] = []
    
    func reset() {
        availableGoals.removeAll()
    }
    
    // MARK: - Auth
    
    func signupUser(email: String, password: String, confirmPassword: String, completion: @escaping (Bool, Error?) -> Void) {
        let url = API.users.signupPath
        let parameters = [API.SharedAPIFields.email: email, API.SharedAPIFields.password: password, API.SignUpFields.confirmPassword: confirmPassword]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default) { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.error)
            }
            PGUser.addCurrentUser(dict)
            completion(true, nil)
        }
    }
    
    func loginUser(_ email: String, password: String, completion: @escaping (Bool, Error?) -> Void) {
        let url = API.users.loginPath
        let parameters = [API.SharedAPIFields.email: email, API.SharedAPIFields.password: password]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default) { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.error)
            }
            PGUser.addCurrentUser(dict)
            completion(true, nil)
        }
    }
    
    func logoutUser(_ deviceToken: String?, completion: @escaping (Bool, Error?) -> Void) {
        let url = API.users.logoutPath
        var parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        if let deviceToken = deviceToken {
            parameters[API.DeviceFields.deviceToken] = deviceToken
        }
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default) { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.error)
            }
            
            completion(true, nil)
        }
    }
    
    func forgotPassword(_ email: String, completion: @escaping (Bool, Error?) -> Void) {
        let url = API.users.forgotPasswordPath
        let parameters = [API.SharedAPIFields.email: email]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.error)
            }
            
            completion(true, nil)
        }
    }
    
    // MARK: - Other
    
    func getPlaidStatus(completion: @escaping (Bool, Bool, Error?, Bool) -> Void) {
        let url = API.plaid.status
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error, false)
            }
            
            if let status = dict[API.PlaidFields.status] as? [[String: Any]], !status.isEmpty, let isIncomeCalculated = status[0][API.PlaidFields.isIncomeCalculated] as? Bool {
                return completion(true, false, nil, isIncomeCalculated)
            }
            
            completion(true, false, nil, false)
        }
    }
    
    func getFinancialInfo(_ recalculate: Bool, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.financialInfosPath
        var parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        if recalculate {
            parameters[API.FinancialInfoFields.recalculate] = true
        }
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGUser.current()!.addFinancialInfos(dict)
            completion(true, false, nil)
        }
    }
    
    func postFinancialInfo(_ id: Int, weeklyAmountCents: Int, completion: @escaping ((Bool, Bool, Error?) -> Void)) {
        let url = API.users.financialInfosPath
        let parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.FinancialInfoFields.financialInfoId: id, API.FinancialInfoFields.weeklyAmountCents: weeklyAmountCents]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            PGUser.current()!.updateFinancialInfo(id, amount: weeklyAmountCents)
            completion(true, false, nil)
        }
    }
    
    func getAvailableGoals(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.admin.goalsPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            if let goals = dict[API.FilterFields.goals] as? [[String: Any]] {
                self.availableGoals = goals
            }
            
            completion(true, false, nil)
        }
    }
    
    func postSurvey(parameters: [String: Any], completion: @escaping (Bool, Bool, Error?, [[String: Any]]?) -> Void) {
        let url = API.users.surveyPath
        var parameters = parameters
        parameters[API.SharedAPIFields.accessToken] = PGUser.current()!.accessToken
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict, let recommendedGoals = dict[API.SharedAPIFields.recommendedGoals] as? [[String: Any]] else {
                return completion(false, validation.suspended, validation.error, nil)
            }
            
            completion(true, false, nil, recommendedGoals)
        }
    }
    
    func getUserGoals(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.goalsPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGGoal.deleteAll()
            
            if let goals = dict[API.FilterFields.goals] as? [[String: Any]] {
                for goal in goals {
                    PGGoal.addGoal(goal)
                }
            }
            
            completion(true, false, nil)
        }
    }
    
    func postUserGoal(_ id: Int, totalAmountCents: Int, description: String, endDate: String?, dueDate: String?, userAccountId: Int?, retirementAccountIds: [Int]?, savingsAmountCents: Int?, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.goalsPath
        var parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.UserGoalsFields.goalId: id, API.UserGoalsFields.totalAmountCents: totalAmountCents, API.UserGoalsFields.description: description]
        if let endDate = endDate {
            parameters[API.UserGoalsFields.endDate] = endDate
        }
        if let dueDate = dueDate {
            parameters[API.UserGoalsFields.dueDate] = dueDate
        }
        if let userAccountId = userAccountId {
            parameters[API.UserGoalsFields.userAccountId] = userAccountId
        }
        if let retirementAccountIds = retirementAccountIds, retirementAccountIds.count > 0 {
            parameters[API.UserGoalsFields.retirementAccountIds] = retirementAccountIds
        }
        if let savingsAmountCents = savingsAmountCents {
            parameters[API.UserGoalsFields.savingsAmountCents] = savingsAmountCents
        }
        
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            if let goal = dict[API.FilterFields.goal] as? [String: Any] {
                PGGoal.addGoal(goal)
            }
            
            completion(true, false, nil)
        }
    }
    
    func updateMultipleUserGoals(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = "\(API.users.goalsPath)"
        let parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.FilterFields.goals: PGGoal.updatedGoalsDicts()]
        AlamofireManager.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGGoal.updateComplete()
            
            if let goals = dict[API.FilterFields.goals] as? [[String: Any]] {
                for goal in goals {
                    PGGoal.addGoal(goal)
                }
            }
            
            completion(true, false, nil)
        }
    }
        
    func updateUserGoal(_ id: Int, totalAmountCents: Int, description: String, endDate: String?, dueDate: String?, userAccountId: Int?, retirementAccountIds: [Int]?, savingsAmountCents: Int?, weeklyAmountCents: Int?, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = "\(API.users.goalsPath)/\(id)"
        var parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.UserGoalsFields.totalAmountCents: totalAmountCents, API.UserGoalsFields.description: description]
        if let endDate = endDate {
            parameters[API.UserGoalsFields.endDate] = endDate
        }
        if let dueDate = dueDate {
            parameters[API.UserGoalsFields.dueDate] = dueDate
        }
        if let userAccountId = userAccountId {
            parameters[API.UserGoalsFields.userAccountId] = userAccountId
        }
        if let retirementAccountIds = retirementAccountIds {
            parameters[API.UserGoalsFields.retirementAccountIds] = retirementAccountIds
        }
        if let savingsAmountCents = savingsAmountCents {
            parameters[API.UserGoalsFields.savingsAmountCents] = savingsAmountCents
        }
        if let weeklyAmountCents = weeklyAmountCents {
            parameters[API.UserGoalsFields.weeklyAmountCents] = weeklyAmountCents
        }
        
        AlamofireManager.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            if let goals = dict[API.FilterFields.goals] as? [[String: Any]] {
                for goal in goals {
                    PGGoal.addGoal(goal)
                }
            }
        
            completion(true, false, nil)
        }
    }
    
    func deleteGoal(_ goal: PGGoal, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = "\(API.users.goalsPath)/\(goal.id)"
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            goal.complete()
            
            completion(true, false, nil)
        }
    }
    
    func getCCTransfer(completion: @escaping (String?, Double?) -> Void) {
        let url = API.users.ccTransferPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict, let dateString = dict[API.NextCCTransferFields.date] as? String, let amountCents = dict[API.NextCCTransferFields.amountCents] as? Double else {
                return completion(nil, nil)
            }
            completion(dateString, amountCents)
        }
    }
    
    func postRetirementCalculator(weeklyIncomeCents: Int, completion: @escaping (Bool, Bool, Error?, Double?) -> Void) {
        let url = API.users.retirementCalculatorPath
        let parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.RetirementCalculatorFields.weeklySalaryCents: weeklyIncomeCents]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict, let output = dict[API.SharedAPIFields.output] as? [String: Any], let retirementAmountCents = output[API.RetirementCalculatorFields.retirementAmountCents] as? Double else {
                return completion(false, validation.suspended, validation.error, nil)
            }
            
            completion(true, false, nil, retirementAmountCents)
        }
    }
    
    func getTransactions(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.fundingSchedulesPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGTransaction.deleteAll()
            
            if let transactions = dict[API.FilterFields.finacialTransactions] as? [[String: Any]] {
                for transaction in transactions {
                    PGTransaction.addTransaction(transaction)
                }
            }
            
            completion(true, false, nil)
        }
    }
    
    func deleteScheduledTransactions(_ date: String, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.scheduledPath
        let parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.DeleteTransactionFields.date: date]
        AlamofireManager.request(url, method: .delete, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGTransaction.deleteScheduled()
            
            completion(true, false, nil)
        }
    }
    
    func postSynapseAccount(_ parameters: [String: Any], completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.synapsePath
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGUser.current()!.addedSynapseAccount()
            
            completion(true, false, nil)
        }
    }
    
    func getAccounts(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.accounts.accountsPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGAccount.deleteAll()
            
            if let accounts = dict[API.FilterFields.accounts] as? [[String: Any]] {
                for account in accounts {
                    PGAccount.addAccount(account)
                }
            }
           
            completion(true, false, nil)
        }
    }
    
    func getAccountTransactions(id: Int, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = "\(API.accounts.accountsPath)/\(id)/transactions"
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }

            PGAccountTransaction.deleteUserAccount(id)
            
            if let transactions = dict[API.FilterFields.transactions] as? [[String: Any]] {
                for transaction in transactions {
                    PGAccountTransaction.addTransaction(transaction)
                }
            }

            completion(true, false, nil)
        }
    }
    
    func postPlaidToken(_ publicToken: String, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.plaidToken.tokenPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.PlaidFields.publicToken: publicToken]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }

            completion(true, false, nil)
        }
    }
    
    func postPrimaryAccount(_ primaryAccountId: String, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.primaryAccountPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.PrimaryAccountFields.accountId: primaryAccountId]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGUser.current()!.addedPrimaryAccount(primaryAccountId)
            
            completion(true, false, nil)
        }
    }
    
    func deleteInstitution(_ institutionId: Int, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.plaid.removeInstitutionPath
        let parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.PlaidFields.userInstitutionId: institutionId]
        AlamofireManager.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            for account in PGAccount.getAssociatedCreditCards(institutionId) {
                PGGoal.completeCreditCardGoals(account.id)
            }
            
            PGAccount.deleteInstitution(institutionId)
            
            completion(true, false, nil)
        }
    }
    
    func closeAccount(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.closeAccountPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            completion(true, false, nil)
        }
    }
    
    func pauseFutureSweeps(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.pausePath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGUser.current()!.pausedUserSweeps(true)
            
            completion(true, false, nil)
        }
    }
    
    func resumeFutureSweeps(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.users.pausePath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, method: .delete, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGUser.current()!.pausedUserSweeps(false)
            
            completion(true, false, nil)
        }
    }
    
    // MARK: - Notifications
    
    func postDevice(_ deviceToken: String) {
        let url = API.users.devicePath
        let parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.DeviceFields.type: API.DeviceFields.apple, API.SharedAPIFields.value: deviceToken]
        AlamofireManager.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                print("APIClient - Error: \(validation.error?.localizedDescription ?? "")")
                return
            }
        }
    }
    
    func getNotifications(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.notifications.notificationsPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGNotification.deleteAll()
 
            if let notifications = dict[API.NotificationsFields.notifications] as? [[String: Any]] {
                for notification in notifications {
                    PGNotification.addNotification(notification)
                }
            }
            
            completion(true, false, nil)
        }
    }
    
    func readNotifications(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.notifications.readNotificationsPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, method: .post, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGNotification.markAllAsRead()
                        
            completion(true, false, nil)
        }
    }
    
    func getSageMessages(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.notifications.sageMessagesPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGSageMessage.deleteAll()
            
            if let messages = dict[API.NotificationsFields.sageMessages] as? [[String: Any]] {
                for message in messages {
                    PGSageMessage.addMessage(message)
                }
            }
            
            completion(true, false, nil)
        }
    }
    
    func readSageMessage(_ message: PGSageMessage, completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.notifications.readSageMessagePath
        let parameters: [String: Any] = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken, API.SharedAPIFields.id: message.id]
        AlamofireManager.request(url, method: .post, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success else {
                return completion(false, validation.suspended, validation.error)
            }
            
            message.markAsRead()
            
            completion(true, false, nil)
        }
    }
    
    func getErrorStates(completion: @escaping (Bool, Bool, Error?) -> Void) {
        let url = API.notifications.errorStatesPath
        let parameters = [API.SharedAPIFields.accessToken: PGUser.current()!.accessToken]
        AlamofireManager.request(url, parameters: parameters)  { (response) in
            let validation = self.validate(response)
            guard validation.success, let dict = validation.dict else {
                return completion(false, validation.suspended, validation.error)
            }
            
            PGErrorState.deleteAll()
            
            if let states = dict[API.NotificationsFields.errorStates] as? [[String: Any]] {
                for state in states {
                    PGErrorState.addState(state)
                }
            }
            
            completion(true, false, nil)
        }
    }
    
    // MARK: - Helper methods
    
    func validate(_ response: DataResponse<Any>) -> (success: Bool, suspended: Bool, error: Error?, dict: [String: Any]?) {
        guard let dict = response.result.value as? [String: Any] else {
            return (false, false, APIError.errorMessage(nil), nil)
        }
        
        guard let success = dict[API.ResultsFields.success] as? Bool, success else {
            let message = dict[API.ResultsFields.error] as? String ?? Constants.ErrorMessages.defaultString
            let suspended = dict[API.ResultsFields.suspended] as? Bool ?? false
            return (false, suspended, APIError.errorMessage(message), nil)
        }
        
        return (true, false, nil, dict)
    }
}

enum APIError: Error {
    case errorMessage(String?)
}

extension APIError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .errorMessage(let message):
            let text = message ?? Constants.ErrorMessages.defaultString
            return NSLocalizedString(text, comment: "")
        }
    }
}
