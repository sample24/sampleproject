//
//  ReusableView.swift
//  CanonPhotoCommunity
//
//  Created by Antonio Duarte on 1/9/18.
//  Copyright © 2018 Canon. All rights reserved.
//

import UIKit

protocol ReusableView: class { }

extension ReusableView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
